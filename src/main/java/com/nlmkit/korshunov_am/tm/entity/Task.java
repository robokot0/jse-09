package com.nlmkit.korshunov_am.tm.entity;

/**
 * Задача
 */
public class Task {
    /**
     * Идентификатор
     */
    private Long id = System.nanoTime();
    /**
     * Имя
     */
    private String name = "";
    /**
     * Описание
     */
    private String description = "";

    /**
     * Констурктор по умолчанию
     */
    public Task() {
    }

    /**
     * Конструктор
     * @param name имя
     */
    public Task(final String name) {
        this.name = name;
    }

    /**
     * Получить идентификатор
     * @return идентификатор
     */
    public long getId() {
        return id;
    }

    /**
     * Установить идентификатор
     * @param id идентификатор
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * Получить име
     * @return имя
     */
    public String getName() {
        return name;
    }

    /**
     * Установить имя
     * @param name имя
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Получить описание
     * @return описание
     */
    public String getDescription() {
        return description;
    }

    /**
     * Указать описание
     * @param description описание
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Получить в виде строки для отображения пользователю
     * @return
     */
    @Override
    public String toString() {
        return id + ": " + name;
    }
}

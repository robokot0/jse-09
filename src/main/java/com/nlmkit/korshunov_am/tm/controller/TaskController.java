package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.service.TaskService;

/**
 * Контроллер задач
 */
public class TaskController extends AbstractController {
    /**
     * Сервис задач
     */
    private final TaskService taskService;

    /**
     * Конструктор
     * @param taskService Сервис задач
     */
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }
    /**
     * Изменить задачу
     * @param task задача
     * @return 0 выполнено
     */
    public int updateTask(final Task task){
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter task description: ");
        final String description = scanner.nextLine();
        taskService.update(task.getId(),name,description);
        System.out.println("[OK]");
        return 0;
    }
    /**
     * Изменить задачу по индексу
     * @return 0 выполнено
     */
    public int updateTaskByIndex(){
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("Please enter task index: ");
        final int index = Integer.parseInt(scanner.nextLine())-1;
        final Task task = taskService.findByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else updateTask(task);
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByName(){
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по ID
     * @return 0 выполнено
     */
    public int removeTaskByID(){
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("Please enter task ID: ");
        final long id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByIndex(){
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("Please enter task index: ");
        final int index = scanner.nextInt()-1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }
    /**
     * Создать задачу
     * @return 0 выполнено
     */
    public int createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter task description: ");
        final String description = scanner.nextLine();
        taskService.create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить все задачи
     * @return 0 выполнено
     */
    public int clearTask(){
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Показать информацию по задаче
     * @param task задача
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Показать задачу по индексу
     * @return 0 выполнено
     */
    public int viewTaskByIndex() {
        System.out.println("Enter, task index:");
        final  int index = scanner.nextInt() - 1;
        final  Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    /**
     * Показать список задач
     * @return 0 выполнено
     */
    public int listTask(){
        System.out.println("[LIST TASK]");

        int index = 1;
        for (final Task task: taskService.findAll()) {
            System.out.println(index + ". " + task.getId()+ ": " + task.getName());
            index ++;
        }

        System.out.println("[OK]");
        return 0;
    }

}

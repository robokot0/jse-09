package com.nlmkit.korshunov_am.tm.controller;

/**
 * Контроллер системных комманд
 */
public class SystemController extends AbstractController {
    /**
     * Показать выход
     * @return 0 выполнено
     */
    public int displayExit(){
        System.out.println("Terminate program");
        return 0;
    }

    /**
     * Показать что была ошибка
     * @return -1 ошибка
     */
    public int displayError(){
        System.out.println("Error! Unknown command.");
        return -1;
    }

    /**
     * Приглашение при входе в программу
     */
    public void displayWelcome(){
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        System.out.println("Enter command");
    }

    /**
     * Показать версию
     * @return 0 выполнено
     */
    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        System.out.println("Short command name in brackets");
        System.out.println("----Common commands:");
        System.out.println("version - Display version (v)");
        System.out.println("about - Display developer info (a)");
        System.out.println("help - Display list of command (h)");
        System.out.println("exit - Terminate console application (e)");
        System.out.println("----Project commands:");
        System.out.println("project-create - Create new project by name. (pcr)");
        System.out.println("project-clear - Remove all projects. (pcl)");
        System.out.println("project-list - Display list of projects. (pl)");
        System.out.println("project-view - View project info. (pv)");
        System.out.println("project-remove-by-id - Remove project by id. (prid)");
        System.out.println("project-remove-by-name - Remove project by name. (prn)");
        System.out.println("project-remove-by-index - Remove project by index. (prin)");
        System.out.println("project-update-by-index - Update name and description of project by index. (puin)");
        System.out.println("----Task commands:");
        System.out.println("task-create - Create new task by name. (tcr)");
        System.out.println("task-clear - Remove all tasks. (tcl)");
        System.out.println("task-list - Display list of tasks. (tl)");
        System.out.println("task-view - View task info. (tv)");
        System.out.println("task-remove-by-id - Remove task by id. (trid)");
        System.out.println("task-remove-by-name - Remove task by name. (trn)");
        System.out.println("task-remove-by-index - Remove task by index. (trin)");
        System.out.println("task-update-by-index - Update name and description of task by index. (tuin)");

        return 0;
    }

    /**
     * Показать информацию о разработчике
     * @return
     */
    public int displayAbout() {
        System.out.println("Andrey Korshunov");
        System.out.println("korshunov_am@nlmk.com");
        return 0;
    }

}
